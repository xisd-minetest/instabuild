--Item Crafting

instabuild.craft.small_hut={
	output = 'instabuild:small_hut 1',
	recipe = {
		{'default:tree', 'default:mese_crystal_fragment','default:tree'},
		{'default:tree', 'doors:door_wood','default:tree'},
		{'default:tree', 'default:wood','default:tree'},
	}
}

instabuild.craft.large_hut={
	output = 'instabuild:large_hut 1',
	recipe = {
		{'default:tree', 'default:mese_crystal_fragment','default:tree'},
		{'default:tree', 'instabuild:small_hut','default:tree'},
		{'default:tree', 'default:wood','default:tree'},
	}
}

instabuild.craft.small_house={
	output = 'instabuild:small_house 1',
	recipe = {
		{'default:brick', 'default:mese_crystal_fragment','default:brick'},
		{'default:brick', 'instabuild:large_hut','default:brick'},
		{'default:brick', 'default:brick','default:brick'},
	}
}

instabuild.craft.large_house={
	output = 'instabuild:large_house 1',
	recipe = {
		{'default:brick', 'default:mese_crystal','default:brick'},
		{'default:brick', 'instabuild:small_house','default:brick'},
		{'default:brick', 'default:brick','default:brick'},
	}
}

instabuild.craft.small_warehouse={
	output = 'instabuild:small_warehouse 1',
	recipe = {
		{'default:wood', 'default:mese_crystal_fragment','default:wood'},
		{'default:wood', 'default:chest','default:wood'},
		{'default:wood', 'default:wood','default:wood'},
	}
}

instabuild.craft.large_warehouse={
	output = 'instabuild:large_warehouse 1',
	recipe = {
		{'default:wood', 'default:mese_crystal','default:wood'},
		{'default:wood', 'instabuild:small_warehouse','default:wood'},
		{'default:wood', 'default:wood','default:wood'},
	}
}

instabuild.craft.small_farm={
	output = 'instabuild:small_farm 1',
	recipe = {
		{'default:papyrus', 'default:mese_crystal_fragment','default:papyrus'},
		{'default:sapling', 'default:dirt','default:sapling'},
		{'default:papyrus', 'default:cactus','default:papyrus'},
	}
}

instabuild.craft.large_farm={
	output = 'instabuild:large_farm 1',
	recipe = {
		{'default:papyrus', 'default:mese_crystal_fragment','default:papyrus'},
		{'default:sapling', 'instabuild:small_farm','default:sapling'},
		{'default:papyrus', 'default:cactus','default:papyrus'},
	}
}

instabuild.craft.short_tower={
	output = 'instabuild:short_tower 1',
	recipe = {
		{'default:cobble', 'default:mese_crystal_fragment','default:cobble'},
		{'default:cobble', 'default:cobble','default:cobble'},
		{'default:cobble', 'doors:door_wood','default:cobble'},

	}
}

instabuild.craft.tall_tower={
	output = 'instabuild:tall_tower 1',
	recipe = {
		{'default:cobble', 'default:mese_crystal','default:cobble'},
		{'default:cobble', 'instabuild:short_tower','default:cobble'},
		{'default:cobble', 'default:cobble','default:cobble'},
	}
}

instabuild.craft.factory={
	output = 'instabuild:factory 1',
	recipe = {
		{'default:glass', 'default:mese_crystal','default:glass'},
		{'default:steel_ingot', 'default:torch','default:steel_ingot'},
		{'default:brick', 'doors:door_wood','default:brick'},
	}
}

instabuild.craft.modern_house={
	output = 'instabuild:modern_house 1',
	recipe = {
		{'default:stonebrick', 'default:stonebrick','default:stonebrick'},
		{'default:stonebrick', 'default:mese_crystal','default:stonebrick'},
		{'doors:door_wood', 'default:stonebrick','default:stonebrick'},
	}
}

instabuild.craft.mansion={
	output = 'instabuild:mansion 1',
	recipe = {
		{'stairs:stair_wood', 'default:wood','stairs:stair_wood'},
		{'default:sand', 'default:mese','default:sand'},
		{'default:wood', 'doors:door_wood','default:wood'},
	}
}

