minetest.register_node("instabuild:glass_panel",{
	drawtype="nodebox",
	description= "Glass Panel",
	paramtype = "light",
	paramtype2 = "facedir",
	walkable = false,
	sunlight_propagates = true,
	tiles = { 'default_glass.png' },
	node_box = {
		type = "fixed",
		fixed = {
			{-0.500000,-0.500000,0.000000,0.500000,0.500000,0.500000},
		},
	},
	groups={oddly_breakable_by_hand=3},
})

minetest.register_node("instabuild:glass_roof",{
	drawtype="nodebox",
	description= "Glass Roof",
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	tiles = { "default_glass.png" },
	node_box = {
		type = "fixed",
		fixed = {
			{-0.500000,-0.500000,-0.500000,-0.125000,-0.125000,0.500000}, 
			{0.125000,0.125000,-0.500000,0.500000,0.500000,0.500000},
			{-0.187500,-0.187500,-0.500000,0.187500,0.187500,0.500000},
		},
	},
	groups={oddly_breakable_by_hand=3},
})

minetest.register_node("instabuild:light",{
	drawtype="nodebox",
	description= "Strip Light",
	light_source = 14,
	tiles = { 'default_wood.png',
	          'default_cloud.png',
			  'default_wood.png',
			  'default_wood.png',
			  'default_wood.png',
			  'default_wood.png' },
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.500000,0.250000,-0.250000,0.500000,0.407216,0.250000},
			{-0.500000,0.312500,-0.062500,0.500000,0.500000,0.062500}, 
		},
	},
	groups={oddly_breakable_by_hand=3},
})

minetest.register_node("instabuild:catwalk",{
	drawtype="nodebox",
	description= "Catwalk",
	paramtype = "light",
	paramtype2 = "facedir",
	tiles = { 'catwalk.png',
	          'catwalk.png',
			  'default_steel_block.png',
			  'default_steel_block.png',
			  'default_steel_block.png',
			  'default_steel_block.png', },
	node_box = {
		type = "fixed",
		fixed = {
			{-0.500000,-0.500000,-0.500000,0.500000,-0.375000,0.500000}, --NodeBox 1
			{-0.500000,-0.500000,-0.062500,-0.437500,0.500000,0.062500}, --NodeBox 2
			{0.443299,-0.500000,-0.062500,0.500000,0.500000,0.062500}, --NodeBox 3
			{0.443299,0.448454,-0.500000,0.500000,0.500000,0.500000}, --NodeBox 4
			{-0.500000,0.448454,-0.500000,-0.437500,0.500000,0.500000}, --NodeBox 5
		},
	},
	groups={oddly_breakable_by_hand=3},
})

minetest.register_node("instabuild:corner",{
	drawtype="nodebox",
	description= "House Corner",
	paramtype = "light",
	sunlight_propagates = true,
	tiles = { 'house_corner.png' },
	groups={oddly_breakable_by_hand=1},
})

minetest.register_node("instabuild:flame", {
	description = "Fake Fire",
	drawtype = "plantlike",
	tiles = {{
		name="fire_basic_flame_animated.png",
		animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1},
	}},
	inventory_image = "fire_basic_flame.png",
	light_source = 14,
	groups = {dig_immediate=3},
	drop = '',
	walkable = false,
	buildable_to = true,
})
