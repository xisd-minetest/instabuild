# instabuild

Edited from instabuild mod by Dan Duncombe
(I could not make a proper fork since the github link is dead)

https://forum.minetest.net/viewtopic.php?f=11&t=6221

"This mod is based heavily on thefamilygrog66's instacabin mod.
The base code for spawning the structures is from worldedit.

It adds several items, which are used to spawn in various types of building.

Protip: as a general rule face along the positive z axis and the buildings should spawn in front of you.

Depends: Default, Worldedit

License: CC-By-SA

Thanks to Evergreen for the V 2.0 Buildings."

-----

I had the need to be able to easily activate / deactivate one model or an other so I spliced the file in tree :

nodes.lua - contains the registered nodes
crafts.lua - contains an array with register craft instructions
init.lua - still has models list and loop but now the loop include minetest.registercraft
( if corresponding instruction exist in craft.lua array )

Now, commenting an element of the list completely deactivate and it can be reactivated as easily 

This edit also add one or two models including one found there :
http://uberi.mesecons.net/projects/Clock/ 
licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. 

-----




